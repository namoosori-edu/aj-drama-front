import {FC, useEffect} from "react";
import {observer, useLocalObservable} from "mobx-react";
import {TeamPageStateKeeper} from "../../../state";
import {DramaException} from "@nara/accent";
import AddressPageDetailView from "./view/AddressPageDetailView";
import React from "react";


const TeamAddressPageDetailContainer = observer(
  ({
     addressPageId,
   }: {
    addressPageId: string;
  }) => {
    //
    const addressPageStateKeeper = useLocalObservable(() => TeamPageStateKeeper.instance);
    const {addressPage} = addressPageStateKeeper;

    useEffect(() => {
      init();
    }, []);

    const init = () => {
      addressPageStateKeeper.findTeamPageById(addressPageId);
    };

    const handleClickDefault = async () => {
      //
      if (!addressPage) {
        throw new DramaException('AddressPageDetail', 'addressPage should not be null.');
      }

      await addressPageStateKeeper.toggleTeamBaseAddress(addressPage);
    }

    return addressPage && (
      <AddressPageDetailView
        addressPage={addressPage}
        onClickDefault={handleClickDefault}
      />
    );
  });


export default TeamAddressPageDetailContainer;
