import {AddressPage} from "../../../../api";
import {observer} from "mobx-react";
import {FC, useState} from "react";
import {Box, Grid, Paper, Typography} from "@mui/material";
import {StarButton, TextAvatar} from "../../../shared";
import React from "react";

const AddressPageDetailView = observer(
  ({
     addressPage,
     onClickDefault,
   }: {
       addressPage: AddressPage;
       onClickDefault: () => void;
     }) => {

    return (
      <Grid container spacing={10}>
        <Grid item xs={3}>
          <Paper sx={{p: 5}}>
            <Box width='100%' display='flex' justifyContent='center'>
              <TextAvatar text={addressPage.name} sx={{width: 175, height: 175}}/>
            </Box>
            <Box mt={5} width='100%' display='flex' justifyContent='space-between' alignItems='center'>
              <Typography variant='h5'>{addressPage.name}</Typography>
              <StarButton
                baseAddress={addressPage.baseAddress}
              />
            </Box>
          </Paper>
        </Grid>
      </Grid>
    )
  }
)

export default AddressPageDetailView;
