import {observer, useLocalObservable} from "mobx-react";
import {FC} from "react";
import {TeamPagesStateKeeper} from "../../../../../state";
import {Box, Button, Divider} from "@mui/material";
import React from "react";

const SortContainer = observer(
  ({}) => {
    //
    const addressPagesStateKeeper = useLocalObservable(() => TeamPagesStateKeeper.instance);
    const {addressBookId} = addressPagesStateKeeper;


    return (
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          width: 'fit-content',
          color: 'text.secondary',
        }}
      >
        <Button
          color="inherit"
        >
          등록일순
        </Button>
        <Divider orientation='vertical' variant='middle' flexItem/>
        <Button
          color='inherit'
        >
          이름순
        </Button>
      </Box>
    );
  });

export default SortContainer;
