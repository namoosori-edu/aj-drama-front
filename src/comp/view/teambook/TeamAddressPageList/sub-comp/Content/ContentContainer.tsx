import {observer, useLocalObservable} from "mobx-react";
import {TeamPageStateKeeper, TeamPagesStateKeeper} from "../../../../../state";
import {FC} from "react";
import {Box, Button, Grid, Pagination, Paper, Typography} from "@mui/material";
import React from "react";
import {Add, ReportSharp} from "@mui/icons-material";
import AddressPageItemView from "./view/AddressPageItemView";


const ContentContainer = observer(
  ({}) => {
    // fixme: addressPageStateKeeper vs teamPagesStateKeeper (변수 네이밍)
    const teamPageStateKeeper = useLocalObservable(() => TeamPageStateKeeper.instance);
    const teamPagesStateKeeper = useLocalObservable(() => TeamPagesStateKeeper.instance);
    const {addressPages, totalCount} = teamPagesStateKeeper;

    return addressPages.length ? (
      <>
        <Grid container spacing={5}>
          {addressPages.map(addressPage => (
            <Grid item xs={3} key={addressPage.id}>
              <AddressPageItemView
                addressPage={addressPage}
              />
            </Grid>
          ))}
        </Grid>
        <Pagination count={totalCount}/>
      </>
    ) : (
      <Paper sx={{p: 10, textAlign: 'center'}}>
        <ReportSharp sx={{fontSize: 120, color: 'lightgrey'}}/>
        <Box>
          <Typography color="lightgrey" variant="subtitle1">"등록된 연락처가 없습니다."</Typography>
          <Typography display="inline" fontWeight="bold" color="grey" variant="subtitle1">연락처 등록하기</Typography>
          <Typography display="inline" color="lightgrey" variant="subtitle1">를 클릭하여 연락처를 등록하시기 바랍니다.</Typography>
        </Box>
        <Button variant="contained" color="secondary" sx={{my: 5}} startIcon={<Add/>}>연락처 등록하기</Button>
      </Paper>
    );
  });

export default ContentContainer;
