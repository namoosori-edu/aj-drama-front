import {FC, useState} from "react";
import {observer} from "mobx-react";
import {AddressPage} from "../../../../../../api";
import {Card, CardActionArea, CardContent, CardHeader, List, ListItem, ListItemIcon, ListItemText} from "@mui/material";
import {ContentPaste, LocationOn} from "@mui/icons-material";
import React from "react";
import {TextAvatar} from "../../../../../shared";


const AddressPageItemView = observer(
  ({
    addressPage,
  }:{
    addressPage: AddressPage;
  }) => {
    const defaultText = '-';
    // const [hovered, setHovered] = useState(false);

    const getTruncatedText = (text: string | null | undefined) => {
      if(!text) {
        return defaultText;
      }

      const maxTextLength = 15;
      const formattedText = text.length > maxTextLength ? `${text.substring(0, maxTextLength)}...` : text;

      return formattedText || defaultText;
    };

    return(
      <Card sx={{ maxWidth: 359 }} elevation={3} >
        <CardActionArea sx={{ p: 1 }}>
          <CardHeader
            avatar={<TextAvatar text={addressPage.name} />}
            title={addressPage.name}
            titleTypographyProps={{ variant: 'h6' }}
          />
          <CardContent sx={{ height: 120, overflow: 'auto' }}>
            <List>
              <ListItem disablePadding>
                <ListItemIcon>
                  <LocationOn sx={{ color: 'lightgrey' }} />
                </ListItemIcon>
                <ListItemText
                  primary={addressPage.address?.fullAddress}
                  primaryTypographyProps={{ variant: 'body2', sx: { color: 'gray' } }}
                />
              </ListItem>
              <ListItem disablePadding>
                <ListItemIcon>
                  <ContentPaste sx={{ color: 'lightgrey' }} />
                </ListItemIcon>
                <ListItemText
                  primary={getTruncatedText(addressPage.memo)}
                  primaryTypographyProps={{ variant: 'body2', sx: { color: 'gray' } }}
                />
              </ListItem>
            </List>
          </CardContent>
        </CardActionArea>
      </Card>
    );
  });

export default AddressPageItemView;
