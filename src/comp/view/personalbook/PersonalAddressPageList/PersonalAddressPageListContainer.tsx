import React, {FC, PropsWithChildren, ReactNode, useEffect} from 'react';
import {observer, useLocalObservable} from 'mobx-react';
import {PersonalPagesStateKeeper} from '../../../state';

const PersonalAddressPageListContainer = observer(
  ({
     addressBookId,
     children,
   }: {
    addressBookId: string;
    children: ReactNode;
  }) => {
    //
    const addressPagesStateKeeper = useLocalObservable(() => PersonalPagesStateKeeper.instance);

    useEffect(() => {
      init();
    }, []);

    const init = () => {
      addressPagesStateKeeper.setAddressBookId(addressBookId);
      addressPagesStateKeeper.findPersonalAddressPages(addressBookId);
    };

    return <>{children}</>;
  });

export default PersonalAddressPageListContainer;
