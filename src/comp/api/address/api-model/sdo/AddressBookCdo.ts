class AddressBookCdo {
  name: string;

  addressBookId: string;
  
  constructor(name: string, addressBookId: string) {
    this.name = name;
    this.addressBookId = addressBookId;
  }

  static new(): AddressBookCdo {
    return new AddressBookCdo('', '');
  }

}

export default AddressBookCdo;
