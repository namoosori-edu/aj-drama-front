import { CommandResponse } from '@nara/accent';
import { ApiClient } from '@nara/prologue';
import {
  AssignPersonalBaseAddressCommand,
  MigratePersonalPageCommand,
  ModifyPersonalBookCommand,
  ModifyPersonalPageCommand,
  RegisterPersonalBookCommand,
  RegisterPersonalPageCommand,
} from '../command';


class PersonalBookFlowApiStub {
  static _instance: PersonalBookFlowApiStub;

  private readonly client = new ApiClient('/api/address-book/feature/personalbook');

  static get instance() {
    if (!PersonalBookFlowApiStub._instance) {
      PersonalBookFlowApiStub._instance = new PersonalBookFlowApiStub();
    }
    return PersonalBookFlowApiStub._instance;
  }

  async registerPersonalAddress(command: RegisterPersonalBookCommand): Promise<CommandResponse> {
    return this.client.postNotNull<CommandResponse>(CommandResponse, '/register-personal-book/command', command);
  }

  async modifyPersonalAddress(command: ModifyPersonalBookCommand): Promise<CommandResponse> {
    return this.client.postNotNull<CommandResponse>(CommandResponse, '/modify-personal-book/command', command);
  }

  async registerPersonalPage(command: RegisterPersonalPageCommand): Promise<CommandResponse> {
    return this.client.postNotNull<CommandResponse>(CommandResponse, '/register-personal-page/command', command);
  }

  async modifyPersonalPage(command: ModifyPersonalPageCommand): Promise<CommandResponse> {
    return this.client.postNotNull<CommandResponse>(CommandResponse, '/modify-personal-page/command', command);
  }

  async assignPersonalBaseAddress(command: AssignPersonalBaseAddressCommand): Promise<CommandResponse> {
    return this.client.postNotNull<CommandResponse>(CommandResponse, '/assign-personal-base-address/command', command);
  }

  async migratePersonalPage(command: MigratePersonalPageCommand): Promise<CommandResponse> {
    return this.client.postNotNull<CommandResponse>(CommandResponse, '/migrate-personal-page/command', command);
  }
}

export default PersonalBookFlowApiStub;
