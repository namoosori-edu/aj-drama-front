import { CommandRequest } from '@nara/accent';


class MigratePersonalPageCommand extends CommandRequest {
  addressBookId: string;

  addressPageId: string;

  constructor(addressBookId: string, addressPageId: string) {
    // super(CommandType.User);
    super();
    this.addressBookId = addressBookId;
    this.addressPageId = addressPageId;
  }

  static new(addressBookId: string, addressPageId: string) {
    const command = new MigratePersonalPageCommand(
      addressBookId,
      addressPageId,
    );

    return command;
  }

}

export default MigratePersonalPageCommand;
