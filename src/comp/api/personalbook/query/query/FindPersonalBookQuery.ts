import { QueryRequest } from '@nara/accent';
import { AddressBook } from '../../../address';


class FindPersonalBookQuery extends QueryRequest<AddressBook> {
  addressBookId: string;

  constructor(addressBookId: string) {
    super(AddressBook);
    this.addressBookId = addressBookId;
  }

  static fromDomain(domain: FindPersonalBookQuery): FindPersonalBookQuery {
    const query = new FindPersonalBookQuery(
      domain.addressBookId,
    );

    query.setResponse(domain);
    return query;
  }

  static byQuery(addressBookId: string) {
    const query = new FindPersonalBookQuery(
      addressBookId,
    );

    return query;
  }

}

export default FindPersonalBookQuery;
