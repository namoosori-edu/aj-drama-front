import { QueryRequest } from '@nara/accent';
import { AddressPage } from '../../../address';


class FindPersonalPagesQuery extends QueryRequest<AddressPage> {
  addressPageId: string;

  constructor(addressPageId: string) {
    super(AddressPage);
    this.addressPageId = addressPageId;
  }

  static fromDomain(domain: FindPersonalPagesQuery): FindPersonalPagesQuery {
    const query = new FindPersonalPagesQuery(
      domain.addressPageId,
    );

    query.setResponse(domain);
    return query;
  }

  static byQuery(addressPageId: string) {
    const query = new FindPersonalPagesQuery(
      addressPageId,
    );

    return query;
  }

}

export default FindPersonalPagesQuery;
