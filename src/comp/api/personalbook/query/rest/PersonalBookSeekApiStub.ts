import { ApiClient } from '@nara/prologue';
import { AddressBook, AddressPage } from '../../../address';
import {
  FindPersonalBookQuery,
  FindPersonalPageQuery,
  FindPersonalPagesPagedQuery,
  FindPersonalPagesQuery,
} from '../query';
import { OffsetElementList } from '@nara/accent';


class PersonalBookSeekApiStub {
  static _instance: PersonalBookSeekApiStub;

  private readonly client = new ApiClient('/api/address-book/feature/personalbook', {
    resDataName: 'queryResult',
  });

  static get instance() {
    if (!PersonalBookSeekApiStub._instance) {
      PersonalBookSeekApiStub._instance = new PersonalBookSeekApiStub();
    }
    return PersonalBookSeekApiStub._instance;
  }

  async findPersonalBook(query: FindPersonalBookQuery): Promise<AddressBook> {
    return this.client.postNotNull<AddressBook>(AddressBook, '/find-personal-book/query', query);
  }

  async findPersonalPages(query: FindPersonalPagesQuery): Promise<AddressPage[]> {
    return this.client.postArray<AddressPage>(AddressPage, '/find-personal-pages/query', query);
  }

  async findPersonalPagesPaged(query: FindPersonalPagesPagedQuery): Promise<OffsetElementList<AddressPage>> {
    return this.client.postOffsetElementList<AddressPage>(AddressPage, '/find-personal-pages-paged/query', query);
  }

  async findPersonalPage(query: FindPersonalPageQuery): Promise<AddressPage> {
    return this.client.postNotNull<AddressPage>(AddressPage, '/find-personal-page/query', query);
  }
}

export default PersonalBookSeekApiStub;
