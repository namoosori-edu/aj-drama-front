import { QueryRequest, Offset } from '@nara/accent';
import { AddressPage } from '../../../address';


class FindTeamPagesPagedQuery extends QueryRequest<AddressPage[]> {
  addressBookId: string;

  offset: Offset;

  constructor(addressBookId: string, offset: Offset) {
    super(AddressPage);
    this.addressBookId = addressBookId;
    this.offset = offset;
  }

  static fromDomain(domain: FindTeamPagesPagedQuery): FindTeamPagesPagedQuery {
    const query = new FindTeamPagesPagedQuery(
      domain.addressBookId,
      domain.offset,
    );

    query.setResponse(domain);
    return query;
  }

  static byQuery(addressBookId: string, offset: Offset) {
    const query = new FindTeamPagesPagedQuery(
      addressBookId,
      offset,
    );

    return query;
  }

}

export default FindTeamPagesPagedQuery;
