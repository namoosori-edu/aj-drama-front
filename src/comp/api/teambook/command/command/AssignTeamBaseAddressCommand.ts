import { CommandRequest } from '@nara/accent';


class AssignTeamBaseAddressCommand extends CommandRequest {
  addressBookId: string;

  addressPageId: string;

  constructor(addressBookId: string, addressPageId: string) {
    // super(CommandType.User);
    super();
    this.addressBookId = addressBookId;
    this.addressPageId = addressPageId;
  }

  static new(addressBookId: string, addressPageId: string) {
    const command = new AssignTeamBaseAddressCommand(
      addressBookId,
      addressPageId,
    );

    return command;
  }

}

export default AssignTeamBaseAddressCommand;
