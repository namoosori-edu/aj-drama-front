import { ApiClient } from '@nara/prologue';
import { CommandResponse } from '@nara/accent';
import {
  AssignTeamBaseAddressCommand,
  MigrateTeamPageCommand,
  ModifyTeamBookCommand,
  ModifyTeamPageCommand,
  RegisterTeamBookCommand,
  RegisterTeamPageCommand,
} from '../command';


class TeamBookFlowApiStub {
  static _instance: TeamBookFlowApiStub;

  private readonly client = new ApiClient('/api/address-book/feature/teambook');

  static get instance() {
    if (!TeamBookFlowApiStub._instance) {
      TeamBookFlowApiStub._instance = new TeamBookFlowApiStub();
    }
    return TeamBookFlowApiStub._instance;
  }

  async registerTeamAddress(command: RegisterTeamBookCommand): Promise<CommandResponse> {
    return this.client.postNotNull<CommandResponse>(CommandResponse, '/register-team-book/command', command);
  }

  async modifyTeamAddress(command: ModifyTeamBookCommand): Promise<CommandResponse> {
    return this.client.postNotNull<CommandResponse>(CommandResponse, '/modify-team-book/command', command);
  }

  async registerTeamPage(command: RegisterTeamPageCommand): Promise<CommandResponse> {
    return this.client.postNotNull<CommandResponse>(CommandResponse, '/register-team-page/command', command);
  }

  async modifyTeamPage(command: ModifyTeamPageCommand): Promise<CommandResponse> {
    return this.client.postNotNull<CommandResponse>(CommandResponse, '/modify-team-page/command', command);
  }

  async assignTeamBaseAddress(command: AssignTeamBaseAddressCommand): Promise<CommandResponse> {
    return this.client.postNotNull<CommandResponse>(CommandResponse, '/assign-team-base-address/command', command);
  }

  async migrateTeamPage(command: MigrateTeamPageCommand): Promise<CommandResponse> {
    return this.client.postNotNull<CommandResponse>(CommandResponse, '/migrate-team-page/command', command);
  }
}

export default TeamBookFlowApiStub;
